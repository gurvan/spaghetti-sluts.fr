/******************************************************************************/
/*  Copyright 2024 Gurvan Debaussart                                          */
/*  This file is distributed under the MIT license.                           */
/******************************************************************************/

package main

import (
  "net/http"
  "spaghetti/show"
)

var (
  shows show.ProcessedFull
  root  string
)

// Serve the processed showlist
func handler_show_list (w http.ResponseWriter, r *http.Request) {
  shows.ShowList.Template.Execute(w, shows.ShowList)
}

// Serve the processed show
func handler_show (w http.ResponseWriter, r *http.Request) {
  if show, ok := shows.Shows.Shows[r.URL.Path]; ok {
    shows.Shows.Template.Execute(w, show)
  } else {
    http.NotFound(w, r)
  }
}

// Serve the file at requested url
func handler_direct (w http.ResponseWriter, r *http.Request) {
  requested := root + "/html" + r.URL.Path
  http.ServeFile(w, r, requested)
}

// Add a ".html" suffix to requested url
func handler_direct_html (w http.ResponseWriter, r *http.Request) {
  requested := root + "/html" + r.URL.Path + ".html"
  http.ServeFile(w, r, requested)
}

// Re-direct to index.html
func handler_index (w http.ResponseWriter, r *http.Request) {
  http.ServeFile(w, r, root + "/html/index.html")
}

