/******************************************************************************/
/*  Copyright 2024 Gurvan Debaussart                                          */
/*  This file is distributed under the MIT license.                           */
/******************************************************************************/

package main

import (
  "log"
  "net/http"
  "os"
  "path/filepath"
  "spaghetti/show"
)

func main() {
  root = filepath.Dir(os.Args[0])
  shows = show.Init(root, root + "/html/show.csv")

  http.HandleFunc("/affiches/", handler_direct)
  http.HandleFunc("/imgs/",     handler_direct)
  http.HandleFunc("/include/",  handler_direct)

  http.HandleFunc("/contact",         handler_direct_html)
  http.HandleFunc("/bonus",           handler_direct_html)
  http.HandleFunc("/livret_paroles",  handler_direct_html)
  http.HandleFunc("/shows-list",      handler_show_list)
  http.HandleFunc("/shows/",          handler_show)

  http.HandleFunc("/", handler_index)

  log.Fatal(http.ListenAndServe(":9991", nil))
}
