/******************************************************************************/
/*  Copyright 2024 Gurvan Debaussart                                          */
/*  This file is distributed under the MIT license.                           */
/******************************************************************************/

package show

import "time"

type Show struct {
  Date
  Heure                       string
  Lieu, Adresse, Ville, Pays  string
  Entree, Prevente            string
  Commentaire                 string
  SetList                     []string
  Affiche                     string
}

func (a Show) Compare (b Show) int {
  return b.Date.Compare(b.Date)
}

func (s Show) isPassed () bool {
  now := timeToDate(time.Now())

  return s.Date.Compare(now) > 0
}

func Init (root, csv string) ProcessedFull {
  shows := showsFromCSV(root, csv)
  return processShows(root, shows)
}

