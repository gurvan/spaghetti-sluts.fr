/******************************************************************************/
/*  Copyright 2024 Gurvan Debaussart                                          */
/*  This file is distributed under the MIT license.                           */
/******************************************************************************/

package show

import (
  "fmt"
  "time"
)

type Date struct {
  Annee, Mois, Jour int // Could be int16 to save up space, or even only one int
}

func timeToDate (t time.Time) Date {
  return Date {
    Annee: t.Year(),
    Mois:  int(t.Month()),
    Jour:  t.Day(),
  }
}

func (d Date) toString () string {
  return fmt.Sprintf("%02d%02d%02d", d.Jour, d.Mois, d.Annee)
}

func (d Date) toURL () string {
  return fmt.Sprintf("/shows/%s", d.toString())
}

func (d Date) Print () {
  println(d.toString())
}

func (a Date) Compare (b Date) int {
  if res := b.Annee - a.Annee; res != 0 {
    return res
  }
  if res := b.Mois - a.Mois; res != 0 {
    return res
  }
  return b.Jour - a.Jour
}
