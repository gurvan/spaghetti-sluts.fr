/******************************************************************************/
/*  Copyright 2024 Gurvan Debaussart                                          */
/*  This file is distributed under the MIT license.                           */
/******************************************************************************/

package show

import (
  "encoding/csv"
  "fmt"
  "log"
  "os"
  "strconv"
  "strings"
)

// Constructors ----------------------------------------------------------------
// These are run once when starting the server

func readCSV (csv_path string) [][]string {
  csv_file, err := os.Open(csv_path)
  if err != nil {
    log.Fatal(err)
  }
  csv_reader := csv.NewReader(csv_file)
  csv_reader.Comma = ';'
  csv_reader.TrimLeadingSpace = true
  records, err := csv_reader.ReadAll()
  if err != nil {
    log.Fatal(err)
  }
  return records[1:] // remove header of column
}

func atoiFatal(s string) int {
  res, err := strconv.Atoi(s)
  if err != nil {
    log.Fatal(err)
  }
  return res
}

func showFromSlice (root string, s []string) Show {
  for i := range s {
    s[i] = strings.TrimSpace(s[i])
  }

  date := Date { atoiFatal(s[0]), atoiFatal(s[1]), atoiFatal(s[2]) }

  var set_list []string
  if s[11] != "" {
    set_list = strings.Split(s[11], ";")
  }

  affiche := fmt.Sprintf("/affiches/%d%02d%02d.avif", date.Annee, date.Mois, date.Jour)
  if _, err := os.ReadFile(root + "/html" + affiche); err != nil {
    affiche = "/imgs/holder.avif"
  }

  return Show {
    Date:         date,
    Heure:        s[3],
    Lieu:         s[4],
    Adresse:      s[5],
    Ville:        s[6],
    Pays:         s[7],
    Entree:       s[8],
    Prevente:     s[9],
    Commentaire:  s[10],
    SetList:      set_list,
    Affiche:      affiche,
  }
}

func showsFromCSV (root, csv_path string) []Show {
  records := readCSV(csv_path)

  res := make([]Show, len(records))
  for i, row := range records {
    res[i] = showFromSlice(root, row)
  }

  return res
}
