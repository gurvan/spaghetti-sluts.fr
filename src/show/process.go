/******************************************************************************/
/*  Copyright 2024 Gurvan Debaussart                                          */
/*  This file is distributed under the MIT license.                           */
/******************************************************************************/

package show

import (
  "html/template"
  "log"
  "codeberg.org/gurvan/dbmath.go/dbmath/ordered_slice"
)

type ProcessedShowList struct {
  Template      *template.Template
  ShowsUpcoming []Show
  ShowsPassed   []Show
}

type ProcessedShows struct {
  Template  *template.Template
  Shows      map[string] Show
}

type ProcessedFull struct {
  ShowList      ProcessedShowList
  Shows         ProcessedShows
}

func processShows (root string, shows []Show) ProcessedFull {
  ordered_slice.Sort(shows)

  // Make the show list --------------------------------------------------------
  shows_list_template, err := template.ParseFiles(root + "/html/shows-list.html")
  if err != nil {
    log.Fatal(err)
  }

  passed_i := len(shows)
  for i, v := range shows {
    if v.isPassed() {
      passed_i = i
      break
    }
  }
  shows_upcoming := shows[:passed_i]
  shows_passed   := shows[passed_i:]

  // Make the show pages -------------------------------------------------------
  show_template, err := template.ParseFiles(root + "/html/show.html")
  if err != nil {
    log.Fatal(err)
  }

  show_map := make(map[string] Show, len(shows))
  for _, v := range shows {
    url := v.Date.toURL()
    show_map[url] = v
  }

  return ProcessedFull {
    ShowList: ProcessedShowList {
      Template:       shows_list_template,
      ShowsPassed:    shows_passed,
      ShowsUpcoming:  shows_upcoming,
    },
    Shows : ProcessedShows {
      Template: show_template,
      Shows:    show_map,
    },
  }
}
