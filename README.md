# www.spaghetti-sluts.fr

## 🔗 Links

* [Website](https://spaghetti-sluts.fr)
* [Bandcamp](https://spaghettisluts.bandcamp.com/)
* [Instagram](https://www.instagram.com/spaghetti__sluts/)
* [Youtube](https://www.youtube.com/channel/UC3NaMSsdXTlwqneWY2Kf9Lg)
* [Deezer](https://www.deezer.com/fr/artist/212890347)
* [Spotify](https://open.spotify.com/artist/061qyGVbQlp75DXT70gdgp)

## 🕺 Adding a show

Please make sure that any poster added is in the `.avif` file format!
If it is not, you can use [ImageMagick](https://imagemagick.org/) to convert it.
Example for converting every file in the working directory to `.avif`:

```sh
mogrify -format avif *.png
```

If possible, compile the server and run it to make sure everything is working
correctly. If your working directory is in `spaghetti-sluts.fr/src`:

```sh
go build && ./spaghetti
```

You can then visit `http://localhost:9991` on your web browser and check that
the show has been added correctly.

## ⚖️ LICENSE

All source code file are distributed under the [MIT license](./LICENSE).
Images are shared under [CC BY-NC-SA 4.0](http://creativecommons.org/licenses/by-nc-sa/4.0/).
